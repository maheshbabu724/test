﻿#I have used ffmpeg tool for joining Video files
#Download ffmpeg tool from https://ffmpeg.zeranoe.com/builds/
#Add bin path in Environmental variables and Run the program

$folderpath = "E:\Scripts\mantra"

$input1="$folderpath\video1.mp4"
$input2="$folderpath\video2.mp4"

$output = "$folderpath\output.mkv"

Function Join-Video ( [Parameter(Mandatory=$True,ValueFromPipeline=$true)] [ValidateNotNull()] $InputFile1,
                     [Parameter(Mandatory=$True,ValueFromPipeline=$true)] [ValidateNotNull()] $InputFile2,
                     [Parameter(Mandatory=$True,ValueFromPipeline=$true)] [ValidateNotNull()] $OutputFile  )
{
    
    if($InputFile1.Trim(" ") -eq "" -or $InputFile2.Trim(" ") -eq "" -or $OutputFile.Trim(" ") -eq "")
    {
        echo "Please provide Valid Input"
        return;
    }
    $fileArray = New-Object System.Collections.ArrayList
    
    $cmdScript = "ffmpeg -i $InputFile1 -f mpegts -c copy $folderpath\file1.mpeg.ts `n"
    $cmdScript += "ffmpeg -i $InputFile2 -f mpegts -c copy $folderpath\file2.mpeg.ts `n"

    $fileArray.Add("$file1")
    $fileArray.Add("$file2")

    $cmdScript+="ffmpeg -isync -i `"concat:$($fileArray -join '|')`" -f matroska -c copy $OutputFile"
    
    Invoke-Expression $cmdScript     
    
}


Join-Video $input1 $input2 $output