﻿function Check-ABC
{
Param ( [Parameter(Mandatory=$True)] [ValidateNotNull()] [string]$a,
        [Parameter(Mandatory=$True)] [ValidateNotNull()] [string]$b,
        [Parameter(Mandatory=$True)] [ValidateNotNull()] [string]$c )

    if($a.Trim(" ") -eq "" -or $b.Trim(" ") -eq "" -or $c.Trim(" ") -eq "")
    {
        echo "Please Provide Valid Parameters"
        return;
    }

    $A_Check = 0
    $B_Check = 0
    $C_Check = 0

    if($a.Contains("A") -or $b.Contains("A") -or $c.Contains("A")) { $A_Check = 1 }
    if($a.Contains("B") -or $b.Contains("B") -or $c.Contains("B")) { $B_Check = 1 }
    if($a.Contains("C") -or $b.Contains("C") -or $c.Contains("C")) { $C_Check = 1 }

    if($A_Check -and $B_Check -and $C_Check) { echo "A B C Values Are Entered" }
    else { echo "Any one of A, B or C Values are Missed" }
}

Check-ABC "A 100" "B 200" "A 200" 
