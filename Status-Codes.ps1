﻿$FileContent = Invoke-WebRequest -URI "https://raw.githubusercontent.com/elastic/examples/master/Common%20Data%20Formats/nginx_logs/nginx_logs"
$dict = @{}

$str = $FileContent.Content
$content = $str.Split('"-"')
$check = 0
foreach($c in $content)
{
    if($check -eq 1)
    {
        $c = $c.Trim(" ")
        $d = $c.Split(" ")
        echo $d[0]
        if($dict.ContainsKey($d[0])) { $dict[$d[0]] += 1 }
        else { $dict.Add($d[0],1) }

        $check = 0
    }

    if($c -like "*HTTP/1.1*") { $check = 1 }
        
}

echo $dict




